import 'dart:io';
import 'dart:isolate';

const port = 8081;

void runServer(_) async {
  print('Running server on ${Isolate.current.debugName}');
  final server =
      await HttpServer.bind(InternetAddress.anyIPv4, port, shared: true);
  await for (final request in server) {
    request.response..write('my response')..close();
  }
}

Future<void> main() async {
  Iterable.generate(Platform.numberOfProcessors, (i) {
    Isolate.spawn(runServer, null, debugName: 'iso-$i');
  }).toList(); // create isolates eagerly

  // block forever
  await Future.delayed(Duration(days: 99999999));
}
