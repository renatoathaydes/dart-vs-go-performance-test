import 'dart:io';

import 'config.dart';
import 'utils.dart';

void runStandardHttpServer(Config config) {
  if (config.staticResponse) {
    _runStandardHttpServerStatic(config);
  } else {
    _runStandardHttpServerDynamic(config);
  }
}

void _runStandardHttpServerStatic(Config config) async {
  final server =
      await HttpServer.bind(config.address, config.port, shared: true);
  await for (final request in server) {
    setHeaders(request.response.headers, helloWorldLength);
    request.response
      ..write(helloWorld)
      ..close();
  }
}

void _runStandardHttpServerDynamic(Config config) async {
  final server =
      await HttpServer.bind(config.address, config.port, shared: true);
  await for (final request in server) {
    final response = 'The time is ${DateTime.now().toIso8601String()}\n';
    setHeaders(request.response.headers, response.length);
    request.response
      ..write(response)
      ..close();
  }
}
