class Config {
  final Object address;
  final int port;
  final bool staticResponse;
  final bool compressed;

  Config({
    required this.address,
    required this.port,
    required this.staticResponse,
    required this.compressed,
  });
}
