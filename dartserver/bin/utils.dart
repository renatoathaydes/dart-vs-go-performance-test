import 'dart:io';

const helloWorld = 'Hello World!\n';
const helloWorldLength = helloWorld.length;

void setHeaders(HttpHeaders headers, int contentLength) {
  headers
    ..removeAll('x-frame-options')
    ..removeAll('x-xss-protection')
    ..removeAll('x-content-type-options')
    ..add('Content-Length', contentLength);
}
