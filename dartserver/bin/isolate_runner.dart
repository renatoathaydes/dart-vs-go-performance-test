import 'dart:io';
import 'dart:isolate';

import 'config.dart';

typedef ServerFunction = Function(Config);

Future<Never> runOnIsolates(ServerFunction function, Config config) async {
  Iterable.generate(Platform.numberOfProcessors, (i) {
    Isolate.spawn(function, config, debugName: 'iso-$i');
  }).toList(); // create isolates eagerly

  // block forever
  await Future.delayed(Duration(days: 99999999));
  throw '';
}
