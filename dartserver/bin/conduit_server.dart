import 'package:conduit/conduit.dart' as conduit;

import 'config.dart';
import 'utils.dart';

void runConduitServer(Config config, int cores) async {
  final app = _createApplication(config.staticResponse)
    ..options.address = config.address
    ..options.port = config.port;

  await app.start(numberOfInstances: cores);

  print('Serving with Conduit at ${app.options.address}:${app.options.port} '
      '${config.staticResponse ? "static" : "dynamic"} response');
}

conduit.Application _createApplication(bool staticResponse) {
  return staticResponse
      ? conduit.Application<StaticBenchmarkChannel>()
      : conduit.Application<DynamicBenchmarkChannel>();
}

class StaticBenchmarkChannel extends conduit.ApplicationChannel {
  @override
  conduit.Controller get entryPoint {
    final router = conduit.Router();

    router
        .route("/")
        .linkFunction((request) async => conduit.Response.ok(helloWorld));

    return router;
  }
}

class DynamicBenchmarkChannel extends conduit.ApplicationChannel {
  @override
  conduit.Controller get entryPoint {
    final router = conduit.Router();

    router.route("/").linkFunction((request) async => conduit.Response.ok(
        'The time is ${DateTime.now().toIso8601String()}\n'));

    return router;
  }
}
