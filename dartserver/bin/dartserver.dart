import 'dart:io';

import 'package:args/args.dart';

import 'alfred.dart';
// import 'conduit_server.dart';
import 'config.dart';
import 'dart_http.dart';
import 'isolate_runner.dart';
import 'jaguar.dart';
import 'shelf.dart';

enum ServerType { alfred, conduit, http, jaguar, shelf }

void main(List<String> arguments) {
  const compressedFlag = 'compress';
  const staticStringFlag = 'static';
  const typeFlag = 'type';
  const addressFlag = 'address';
  const portFlag = 'port';
  const helpFlag = 'help';

  final parser = ArgParser()
    ..addFlag(helpFlag, abbr: 'h')
    ..addFlag(compressedFlag)
    ..addFlag(staticStringFlag)
    ..addOption(typeFlag,
        allowed: ServerType.values.map((e) => e.name), defaultsTo: 'http')
    ..addOption(addressFlag, help: 'Local address for connection')
    ..addOption(portFlag, help: 'Port for connection', defaultsTo: '8080');

  final argResults = parser.parse(arguments);

  if (argResults.wasParsed(helpFlag)) {
    return print('Usage:\n${parser.usage}');
  }

  final config = Config(
      compressed: argResults[compressedFlag] as bool,
      staticResponse: argResults[staticStringFlag] as bool,
      address: argResults[addressFlag] ?? InternetAddress.anyIPv6,
      port: int.parse(argResults[portFlag]));

  final serverType = ServerType.values.byName(argResults[typeFlag]);

  switch (serverType) {
    case ServerType.alfred:
      runOnIsolates(runAlfredServer, config);
      break;
    case ServerType.http:
      runOnIsolates(runStandardHttpServer, config);
      break;
    case ServerType.jaguar:
      runOnIsolates(runJaguarServer, config);
      break;
    case ServerType.shelf:
      runOnIsolates(runShelfServer, config);
      break;
    case ServerType.conduit:
      // takes care of running in Isolates by itself
      // commented out because it causes startup to become much slower
      throw Exception('Conduit is commented out!');
    // runConduitServer(config, Platform.numberOfProcessors);
    // break;
  }
}
