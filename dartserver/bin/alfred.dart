import 'package:alfred/alfred.dart';

import 'config.dart';
import 'utils.dart';

void runAlfredServer(Config config) async {
  final server = Alfred(logLevel: LogType.error);
  server.get('/', config.staticResponse ? _staticResponse : _dynamicResponse);
  await server.listen(config.port, config.address);
  print('Serving with Alfred at http://${config.address}:${config.port} '
      '${config.staticResponse ? "static" : "dynamic"} response');
}

String _staticResponse(HttpRequest req, HttpResponse res) {
  res.headers.date = DateTime.now().toUtc();
  res.contentLength = helloWorldLength;
  setHeaders(res.headers, helloWorldLength);
  return helloWorld;
}

String _dynamicResponse(HttpRequest req, HttpResponse res) {
  res.headers.date = DateTime.now().toUtc();
  final response = 'The time is ${DateTime.now().toIso8601String()}\n';
  res.contentLength = response.length;
  setHeaders(res.headers, response.length);
  return response;
}
