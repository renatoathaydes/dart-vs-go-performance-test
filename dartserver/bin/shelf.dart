import 'dart:io';

import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as shelf_io;

import 'config.dart';

void runShelfServer(Config config) async {
  var handler = const shelf.Pipeline().addHandler(config.staticResponse
      ? (request) => shelf.Response.ok('Hello World!\n',
          headers: const {HttpHeaders.serverHeader: ''})
      : (request) => shelf.Response.ok(
          'The time is ${DateTime.now().toIso8601String()}\n'));

  var server =
      await shelf_io.serve(handler, config.address, config.port, shared: true);
  server.autoCompress = config.compressed;
  server.defaultResponseHeaders
    ..removeAll('x-frame-options')
    ..removeAll('x-xss-protection')
    ..removeAll('x-content-type-options');

  print('Serving with Shelf at http://${server.address.host}:${server.port}'
      ', ${config.compressed ? "with" : "without"} compression '
      '${config.staticResponse ? "static" : "dynamic"} response');
}
