import 'package:jaguar/jaguar.dart';
import 'utils.dart';

import 'config.dart';

void runJaguarServer(Config config) async {
  print('Serving with Jaguar at http://${config.address}:${config.port} '
      '${config.staticResponse ? "static" : "dynamic"} response');

  final server = Jaguar(multiThread: true);
  server.get(
      '/',
      config.staticResponse
          ? _staticResponse
          : (context) => 'The time is ${DateTime.now().toIso8601String()}\n');

  await server.serve();
}

String _staticResponse(Context context) {
  context.response.headers.add('content-length', helloWorldLength);
  return helloWorld;
}
